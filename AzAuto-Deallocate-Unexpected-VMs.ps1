param (
    [string]$spCredentialName = $(throw "Need name of credential from Automation Account."),
    [string]$tenantId = $(throw "Need tenant ID."),
    [string]$resourceGroupName = $(throw "Need resource group name."),
    [string]$expectedPattern = $(throw "Need some expected patterns to compare against VM names.")
)

$psCredentials = Get-AutomationPSCredential -Name $spCredentialName

Connect-AzAccount -Credential $psCredentials -Tenant $tenantId -ServicePrincipal

$discoveredVMs = Get-AzVM -ResourceGroupName $resourceGroupName | Where-Object {$_.Name -NotMatch $expectedPattern} | Select-Object -Property Name

foreach ($vm in $discoveredVMs) {
    Write-Output ($vm.Name + " didn't meet the pattern. Stopping it...")
    Stop-AzVm -Force -ResourceGroupName $resourceGroupName -Name $vm.Name
}