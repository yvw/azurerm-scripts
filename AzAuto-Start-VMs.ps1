param (
    [string]$spCredentialName = $(throw "Need name of credential from Automation Account."),
    [string]$tenantId = $(throw "Need tenant ID."),
    [string]$resourceGroupName = $(throw "Need resource group name."),
    [string]$vmNamePattern = $(throw "Need some patterns to discover VMs.")
)

$psCredentials = Get-AutomationPSCredential -Name $spCredentialName

Connect-AzAccount -Credential $psCredentials -Tenant $tenantId -ServicePrincipal

$discoveredVMs = Get-AzVM -ResourceGroupName $resourceGroupName | Where-Object {$_.Name -Match $vmNamePattern} | Select-Object -Property Name

foreach ($vm in $discoveredVMs) {
    Write-Output ($vm.Name + " met the pattern. Starting it...")
    Start-AzVm -ResourceGroupName $resourceGroupName -Name $vm.Name
}