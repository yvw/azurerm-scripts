param (
    [string]$spCredentialName = $(throw "Need name of Service Principal credential from Automation Account."),
    [string]$smtpCredentialName = $(throw "Need name of SMTP credential from Automation Account."),
    [string]$tenantId = $(throw "Need tenant ID."),
    [string]$resourceGroupName = $(throw "Need resource group name."),
    [string]$vmNamePattern = $(throw "Need an expected pattern of VM names to check."),
    [byte]$vmThreshold = $(throw "Need a threshold of VMs allowed."),
    [string]$alertEmailFrom = $(throw "Need the email address from which to send alert."),
    [string[]]$alertEmailTo = $(throw "Need the email address to send alert."),
    [string]$alertEmailSmtpSrv = $(throw "Need the SMTP server for sending alert.")
)

$psCredentials = Get-AutomationPSCredential -Name $spCredentialName
$smtpCredentials = Get-AutomationPSCredential -Name $smtpCredentialName

Connect-AzAccount -Credential $psCredentials -Tenant $tenantId -ServicePrincipal

$discoveredVMs = Get-AzVM -ResourceGroupName $resourceGroupName | Where-Object {$_.Name -Match $vmNamePattern} | Select-Object -Property Name

if ($discoveredVMs.Count -ge $vmThreshold) {
    Write-Output "Discovered enough VMs to alert. Sending alert..."

    $alertBody = "Resource Group: $resourceGroupName`nVM Threshold: $vmThreshold`nVM Pattern: $vmNamePattern`n`nVMs Found:`n"

    foreach ($vm in $discoveredVMs) {
        $alertBody += "$($vm.Name)`n"
    }

    Send-MailMessage -From $alertEmailFrom `
        -To $alertEmailTo `
        -Subject "Azure alert: Number of VMs at or beyond threshold" `
        -SmtpServer $alertEmailSmtpSrv `
        -Credential $smtpCredentials `
        -UseSsl `
        -Body $alertBody
}