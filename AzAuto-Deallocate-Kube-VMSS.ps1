param (
    [string]$spCredentialName = $(throw "Need name of credential from Automation Account."),
    [string]$tenantId = $(throw "Need tenant ID."),
    [string]$resourceGroupName = $(throw "Need resource group name.")
)

$psCredentials = Get-AutomationPSCredential -Name $spCredentialName

Connect-AzAccount -Credential $psCredentials -Tenant $tenantId -ServicePrincipal

$scaleSetName = Get-AzVmss -ResourceGroupName $resourceGroupName | Select-Object -First 1 -ExpandProperty Name

Stop-AzVmss -Force -ResourceGroupName $resourceGroupName -VMScaleSetName $scaleSetName