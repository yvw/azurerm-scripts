param (
    [string]$spCredentialName = $(throw "Need name of credential from Automation Account."),
    [string]$bitbucketCredentialName = $(throw "Need name of Bitbucket auth credential from Automation Account."),
    [string]$smtpCredentialName = $(throw "Need name of SMTP credential from Automation Account."),
    [string]$tenantId = $(throw "Need tenant ID."),
    [string]$resourceGroupName = $(throw "Need resource group name."),
    [string]$vmNamePattern = $(throw "Need some expected patterns to compare against VM names."),
    [string]$repoFullName = $(throw "Need the full name of the Bitbucket repo (workspace/repo)."),
    [string]$alertEmailFrom = $(throw "Need the email address from which to send alert."),
    [string[]]$alertEmailTo = $(throw "Need the email address to send alert."),
    [string]$alertEmailSmtpSrv = $(throw "Need the SMTP server for sending alert.")
)

$psCredentials = Get-AutomationPSCredential -Name $spCredentialName
$bitbucketCreds = Get-AutomationPSCredential -Name $bitbucketCredentialName
$smtpCredentials = Get-AutomationPSCredential -Name $smtpCredentialName

Connect-AzAccount -Credential $psCredentials -Tenant $tenantId -ServicePrincipal

$discoveredVMs = Get-AzVM -ResourceGroupName $resourceGroupName | Where-Object {$_.Name -Match $vmNamePattern} | Select-Object -Property Name, Tags

$msg = @()

foreach ($vm in $discoveredVMs) {
    if ([string]::IsNullOrEmpty($vm.Tags['branch'])) {
        $m = "$($vm.Name) has no branch tag."
        Write-Output $m
        $msg += $m
    } else {
        # PowerShell 7.0+:
        #$branchesResponse = Invoke-RestMethod -Credential $bitbucketCreds -Authentication "Basic" -Uri "https://api.bitbucket.org/2.0/repositories/$repoFullName/refs/branches"
        
        # PowerShell 5.1:
        $b64Creds = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("$($bitbucketCreds.GetNetworkCredential().username):$($bitbucketCreds.GetNetworkCredential().password)")))
        $branchesResponse = Invoke-RestMethod -Headers @{Authorization=("Basic $b64Creds")} -Uri "https://api.bitbucket.org/2.0/repositories/$repoFullName/refs/branches?pagelen=50"

        if (!($branchesResponse.values | Where-Object {$_.name -Match "$($vm.Tags['branch'])"})) {
            $m = "$($vm.Name) is tagged with a branch that is not active in Bitbucket."
            Write-Output $m
            $msg += $m
        } else {
            Write-Output "$($vm.Name) is tagged with an active branch. Yay."
        }
    }
}

if ($msg.Count -gt 0) {
    Write-Output "Discovered incorrect branch tags. Sending alert..."

    $alertBody = "Resource Group: $resourceGroupName`nVM Pattern: $vmNamePattern`n`nVMs Found:`n"

    foreach ($m in $msg) {
        $alertBody += "$m`n"
    }

    Send-MailMessage -From $alertEmailFrom `
        -To $alertEmailTo `
        -Subject "Azure alert: VMs have incorrect branch tags" `
        -SmtpServer $alertEmailSmtpSrv `
        -Credential $smtpCredentials `
        -UseSsl `
        -Body $alertBody
}