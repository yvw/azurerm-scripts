param (
    [string]$spCredentialName = $(throw "Need name of credential from Automation Account."),
    [string]$tenantId = $(throw "Need tenant ID."),
    [string]$resourceGroupName = $(throw "Need resource group name."),
    [string]$diskNamePattern = $(throw "Need an expected pattern to compare against disk names.")
)

$psCredentials = Get-AutomationPSCredential -Name $spCredentialName

Connect-AzAccount -Credential $psCredentials -Tenant $tenantId -ServicePrincipal

$disks = Get-AzDisk -ResourceGroupName $resourceGroupName | Where-Object {$_.Name -Match $diskNamePattern} | Select-Object -Property Name, Id

foreach ($disk in $disks) {
	Write-Output "$($disk.Name) met pattern. Creating snapshot..."

	$snapshot =  New-AzSnapshotConfig -SourceUri $disk.Id -Location "Australia Southeast" -CreateOption copy
	
	New-AzSnapshot -Snapshot $snapshot -SnapshotName "$($disk.Name)-snapshot-$(Get-Date -Format yyyyMMddhhmmss)" -ResourceGroupName $resourceGroupName
}