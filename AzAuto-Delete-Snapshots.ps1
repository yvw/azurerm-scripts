param (
    [string]$spCredentialName = $(throw "Need name of credential from Automation Account."),
    [string]$tenantId = $(throw "Need tenant ID."),
    [string]$resourceGroupName = $(throw "Need resource group name."),
    [string]$snapshotNamePattern = $(throw "Need an expected pattern to compare against disk names."),
    [string]$keepPeriod  = $(throw "Need a period (day, month) to compare against disk ages."),
    [byte]$keepValue = $(throw "Need a value of periods to compare against disk ages.")
)

if ("day", "month" -notcontains $keepPeriod.ToLower()) {
    throw "'$($keepPeriod)' is not a valid value. Should be day or month."
}

Write-Output "Snapshot Name Pattern: $($snapshotNamePattern)"
Write-Output "Older Than Period: $($keepPeriod)"
Write-Output "Older Than Value: $($keepValue)"

switch ($keepPeriod) {
    "day" {
        $deleteOlderThan = (Get-Date).AddDays("-$($keepValue)")
        break
    }
    "month" {
        $deleteOlderThan = (Get-Date).AddMonths("-$($keepValue)")
        break
    }
}

Write-Output "Older Than Date: $($deleteOlderThan)"

$psCredentials = Get-AutomationPSCredential -Name $spCredentialName

Connect-AzAccount -Credential $psCredentials -Tenant $tenantId -ServicePrincipal

$snapshots = Get-AzSnapshot -ResourceGroupName $resourceGroupName | Where-Object {$_.Name -Match $snapshotNamePattern} | Select-Object -Property Name, TimeCreated

foreach ($snapshot in $snapshots) {
    if ($snapshot.TimeCreated -lt $deleteOlderThan) {
        Write-Output "$($snapshot.Name) was created $($snapshot.TimeCreated) and meets the criteria for deletion. Removing..."

        Remove-AzSnapshot -ResourceGroupName $resourceGroupName -SnapshotName $snapshot.Name -Force
    }
}