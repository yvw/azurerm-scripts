param (
    [string]$spCredentialName = $(throw "Need name of credential from Automation Account."),
    [string]$tenantId = $(throw "Need tenant ID."),
    [string]$resourceGroupName = $(throw "Need resource group name."),
    [string]$vmName = $(throw "Need VM name.")
)

$psCredentials = Get-AutomationPSCredential -Name $spCredentialName

Connect-AzAccount -Credential $psCredentials -Tenant $tenantId -ServicePrincipal

Stop-AzVm -Force -ResourceGroupName $resourceGroupName -Name $vmName